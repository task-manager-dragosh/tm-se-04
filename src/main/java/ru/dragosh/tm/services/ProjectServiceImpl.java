package ru.dragosh.tm.services;

import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.repos.interfaces.ProjectRepo;
import ru.dragosh.tm.services.interfaces.ProjectService;

import java.util.List;

import static ru.dragosh.tm.utils.OutputUtils.log;

public class ProjectServiceImpl implements ProjectService {
    private ProjectRepo projectRepo;
    public ProjectServiceImpl(final ProjectRepo projectRepo) {
        this.projectRepo = projectRepo;
    }

    @Override
    public List<Project> findAll() {
        return projectRepo.findAll();
    }

    @Override
    public Project find(final String projectName) {
        if (projectName != null && !projectName.isEmpty())
            return projectRepo.find(projectName);
        return null;
    }

    @Override
    public boolean persist(final Project project) {
        if (project != null &&
                project.getName() != null && !project.getName().isEmpty() &&
                project.getDescription() != null && !project.getDescription().isEmpty() &&
                project.getDateStart() != null && project.getDateFinish() != null) {
            Project existingProject = projectRepo.find(project.getName());
            if (existingProject == null) {
                projectRepo.persist(project);
                return true;
            } else
                log("MESSAGE", "Проект с таким названием уже существует!");
        }
        return false;
    }

    @Override
    public void merge(final String projectName, final String newProjectName) {
        if (projectName != null && !projectName.isEmpty() &&
                newProjectName != null && !newProjectName.isEmpty()) {
            Project existingProject = projectRepo.find(newProjectName);
            Project project = projectRepo.find(projectName);
            if (existingProject != null) {
                log("MESSAGE", "Проект с таким название уже существует!");
                return;
            }
            if (project != null) {
                project.setName(newProjectName);
                projectRepo.merge(project);
            }
        }
    }

    @Override
    public void remove(final String projectName) {
        if (projectName != null && !projectName.isEmpty()) {
            Project existingProject = projectRepo.find(projectName);
            if (existingProject != null) {
                projectRepo.remove(projectName);
            } else
                log("MESSAGE", "Проект с таким названием не найден!");
            projectRepo.remove(projectName);
        }
    }

    @Override
    public void removeAll() {
        projectRepo.removeAll();
    }
}
