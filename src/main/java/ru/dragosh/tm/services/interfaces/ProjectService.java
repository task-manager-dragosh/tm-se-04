package ru.dragosh.tm.services.interfaces;

import ru.dragosh.tm.entity.Project;

import java.util.List;

public interface ProjectService {
    List<Project> findAll();
    Project find(String projectName);
    boolean persist(Project project);
    void merge(String projectName, String newProjectName);
    void remove(String projectName);
    void removeAll();
}
