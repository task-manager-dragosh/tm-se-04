package ru.dragosh.tm.services.interfaces;

import ru.dragosh.tm.entity.Task;

import java.util.List;

public interface TaskService {
    List<Task> findAll(String ipProject);
    Task find(String projectId, String nameTask);
    boolean persist(Task task);
    void merge(String projectId, String taskName, String newTaskName);
    void remove(String projectId, String nameTask);
    void removeAll(String projectId);
}
