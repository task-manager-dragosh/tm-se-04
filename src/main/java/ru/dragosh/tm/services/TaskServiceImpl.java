package ru.dragosh.tm.services;

import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.repos.interfaces.TaskRepo;
import ru.dragosh.tm.services.interfaces.TaskService;

import java.util.List;

import static ru.dragosh.tm.utils.OutputUtils.log;

public class TaskServiceImpl implements TaskService {
    private TaskRepo taskRepo;
    public TaskServiceImpl(TaskRepo taskRepo) {
        this.taskRepo = taskRepo;
    }

    @Override
    public List<Task> findAll(final String idProject) {
        return taskRepo.findAll(idProject);
    }

    @Override
    public Task find(final String projectId, final String nameTask) {
        return taskRepo.find(projectId, nameTask);
    }

    @Override
    public boolean persist(final Task task) {
        if (task != null &&
                task.getNameTask() != null && !task.getNameTask().isEmpty() &&
                task.getProjectId() != null && !task.getProjectId().isEmpty() &&
                task.getDescription() != null && !task.getDescription().isEmpty() &&
                task.getDateStart() != null && task.getDateFinish() != null) {
            Task existingTask = taskRepo.find(task.getProjectId(), task.getNameTask());
            if (existingTask == null)
                taskRepo.persist(task);
            else
                log("MESSAGE", "Проект с таким именем уже существует!");
        } else
            log("ERROR", "Ошибка в форматах данных объекта task!");
        return false;
    }

    @Override
    public void merge(final String projectId, final String taskName, final String newTaskName) {
        if (projectId != null && !projectId.isEmpty()
                && taskName != null && ! taskName.isEmpty()
                && newTaskName != null && !newTaskName.isEmpty()) {
            Task task = taskRepo.find(projectId, taskName);
            Task existingTask = taskRepo.find(projectId, newTaskName);
            if (existingTask == null) {
                task.setName(newTaskName);
                taskRepo.merge(task);
            } else
                log("MESSAGE", "Задача с таким названием уже существует!");
        } else
            log("ERROR", "Ошибка в форматах данных объекта task!");
    }

    @Override
    public void remove(final String projectId, final String taskName) {
        if (taskName != null && !taskName.isEmpty()) {
            Task task = taskRepo.find(projectId, taskName);
            if (task != null)
                taskRepo.remove(taskName);
            else
                log("MESSAGE", "Задача с таким названием не найдена!");
        } else
            log("ERROR", "Ошибка в форматах данных объекта task!");
    }

    @Override
    public void removeAll(final String projectId) {
        if (projectId != null && !projectId.isEmpty()) {
            taskRepo.removeAll(projectId);
        }
    }
}
