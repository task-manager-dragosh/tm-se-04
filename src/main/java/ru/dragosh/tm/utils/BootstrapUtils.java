package ru.dragosh.tm.utils;

import java.util.ArrayList;
import java.util.List;

public final class BootstrapUtils {
    public static final String WELCOME_MESSAGE = "*** WELCOME TO TASK MANAGER ***";
    public static final String HELP_MESSAGE = "Для получения подробной справки по командам введите 'help'";
    //User commands
    public static final String HELP = "help";
    public static final String FIND_PROJECT = "find project";
    public static final String FIND_ALL_PROJECTS = "find all projects";
    public static final String FIND_TASK = "find task";
    public static final String FIND_ALL_TASKS = "find all tasks";
    public static final String ADD_PROJECT = "add project";
    public static final String ADD_TASK = "add task";
    public static final String CHANGE_PROJECT_NAME = "change project name";
    public static final String CHANGE_TASK_NAME = "change task name";
    public static final String REMOVE_PROJECT = "remove project";
    public static final String REMOVE_TASK = "remove task";
    public static final String REMOVE_ALL_PROJECTS = "remove all projects";
    public static final String REMOVE_ALL_TASKS = "remove all tasks";
    public static final String EXIT = "exit";

    private static final String DESCRIPTION_FIND_PROJECT = " (находит и выводит на экран проект из базы данных Projects)";
    private static final String DESCRIPTION_FIND_ALL_PROJECTS = " (находит и выводит на экран все проекты из базы данных Projects)";
    private static final String DESCRIPTION_FIND_TASK = " (находит и выводит на экран задачу по определенному проекту из базы данных Tasks)";
    private static final String DESCRIPTION_FIND_ALL_TASKS = " (находит и выводит на экран все задачи по определенному проекту из базы данных Tasks)";
    private static final String DESCRIPTION_ADD_PROJECT = " (добавляет в Project новый проект)";
    private static final String DESCRIPTION_ADD_TASK = " (добавляет в проект новую задачу)";
    private static final String DESCRIPTION_CHANGE_PROJECT_NAME = " (изменяет название определенного проекта в базе данных Projects)";
    private static final String DESCRIPTION_CHANGE_TASK_NAME = " (изменяет название задачи из определенного проекта в базе данных Tasks)";
    private static final String DESCRIPTION_REMOVE_PROJECT = " (удаляет из Project base проект)";
    private static final String DESCRIPTION_REMOVE_TASK = " (удаляет из проекта задачу)";
    private static final String DESCRIPTION_REMOVE_ALL_PROJECTS = " (удаляет все проекты и связанные с ними задачи из баз данных Projects и Tasks)";
    private static final String DESCRIPTION_REMOVE_ALL_TASKS = " (удаляет все задачи по определенному проекту из базы данных Tasks)";
    private static final String DESCRIPTION_EXIT = " (после завершения ввода названия/названий введите \"exit\" + Enter)";


    public static List<String> commandsList = new ArrayList<String>(){{
        add(FIND_PROJECT + DESCRIPTION_FIND_PROJECT);
        add(FIND_ALL_PROJECTS + DESCRIPTION_FIND_ALL_PROJECTS);
        add(FIND_TASK + DESCRIPTION_FIND_TASK);
        add(FIND_ALL_TASKS + DESCRIPTION_FIND_ALL_TASKS);
        add(ADD_PROJECT + DESCRIPTION_ADD_PROJECT + DESCRIPTION_EXIT);
        add(ADD_TASK + DESCRIPTION_ADD_TASK + DESCRIPTION_EXIT);
        add(CHANGE_PROJECT_NAME + DESCRIPTION_CHANGE_PROJECT_NAME);
        add(CHANGE_TASK_NAME + DESCRIPTION_CHANGE_TASK_NAME);
        add(REMOVE_PROJECT + DESCRIPTION_REMOVE_PROJECT);
        add(REMOVE_TASK + DESCRIPTION_REMOVE_TASK);
        add(REMOVE_ALL_PROJECTS + DESCRIPTION_REMOVE_ALL_PROJECTS);
        add(REMOVE_ALL_TASKS + DESCRIPTION_REMOVE_ALL_TASKS);
        add(EXIT + DESCRIPTION_EXIT);
    }};
}
