package ru.dragosh.tm.utils;

import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class OutputUtils {
    private final static Map<String, String> borders = new HashMap<String, String>() {{
        put("PROJECT_BORDER", "**** PROJECT  ****");
        put("PROJECTS_BORDER", "**** PROJECTS ****");
        put("TASK_BORDER", "****   TASK   ****");
        put("TASKS_BORDER", "****  TASKS   ****");
        put("END_BORDER", "******************");

    }};
    private final static Map<String, String> logTypes = new HashMap<String, String>() {{
        put("MESSAGE", "MESSAGE -> ");
        put("ERROR", "ERROR -> ");
    }};

    public static void log(String type, String message) {
        System.err.println(logTypes.get(type) + message);
    }

    public static void projectOutput(Project project) {
        System.out.println(borders.get("PROJECT_BORDER"));
        System.out.println(project);
        System.out.println(borders.get("END_BORDER"));
    }

    public static void taskOutput(Task task) {
        System.out.println(borders.get("TASK_BORDER"));
        System.out.println(task);
        System.out.println(borders.get("END_BORDER"));
    }

    public static void tasksOutput(List<Task> tasks) {
        tasks.forEach(task -> {
            System.out.println(borders.get("TASK_BORDER"));
            System.out.println(task);
            System.out.println(borders.get("END_BORDER"));
        });
    }
}
