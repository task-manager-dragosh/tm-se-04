package ru.dragosh.tm.bootstrap.intetfaces;

public interface CommandPerformer {
    void findProject();
    void findAllProjects();
    void findTask();
    void findAllTasks();
    void addProject();
    void addTask();
    void changeProjectName();
    void changeTaskName();
    void removeProject();
    void removeAllProjects();
    void removeTask();
    void removeAllTasks();
    String readWord(String message);
    void printHelp();
}
