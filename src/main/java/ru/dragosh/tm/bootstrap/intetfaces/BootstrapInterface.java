package ru.dragosh.tm.bootstrap.intetfaces;

public interface BootstrapInterface {
    void init();
    String readCommand();
}
