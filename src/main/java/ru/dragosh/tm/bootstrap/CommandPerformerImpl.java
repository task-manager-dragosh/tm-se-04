package ru.dragosh.tm.bootstrap;

import ru.dragosh.tm.bootstrap.intetfaces.CommandPerformer;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.services.interfaces.ProjectService;
import ru.dragosh.tm.services.interfaces.TaskService;

import java.text.ParseException;
import java.util.List;
import java.util.Scanner;

import static ru.dragosh.tm.utils.BootstrapUtils.commandsList;
import static ru.dragosh.tm.utils.OutputUtils.*;

public class CommandPerformerImpl implements CommandPerformer {
    private ProjectService projectService;
    private TaskService taskService;

    public CommandPerformerImpl(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void findProject() {
        String projectName = readWord("Введите название проекта: ");
        Project project = projectService.find(projectName);
        if (project != null) {
            List<Task> taskList = taskService.findAll(project.getId());
            projectOutput(project);
            tasksOutput(taskList);
        } else
            log("MESSAGE", "Проект с таким названием не найден!");
    }

    @Override
    public void findAllProjects() {
        List<Project> projectList = projectService.findAll();
        projectList.forEach(project -> {
            projectOutput(project);
            List<Task> taskList = taskService.findAll(project.getId());
            tasksOutput(taskList);
        });
    }

    @Override
    public void findTask() {
        String projectName = readWord("Введите название проекта: ");
        /**
         * Костыль
         */
        Project project = projectService.find(projectName);
        Task task = null;
        if (project != null) {
            String taskName = readWord("Введите название задачи: ");
            task = taskService.find(project.getId(), taskName);
            if (task != null) {
                projectOutput(project);
                taskOutput(task);
            } else
                log("MESSAGE", "Задача с таким названием не найдена!");
        }
    }

    @Override
    public void findAllTasks() {
        String projectName = readWord("Введите название проекта: ");
        /**
         * Костыль
         */
        Project project = projectService.find(projectName);
        List<Task> taskList = null;
        if (project != null) {
            taskList = taskService.findAll(project.getId());
            projectOutput(project);
            tasksOutput(taskList);
        } else
            log("MESSAGE", "Проект с таким названием не найден!");
    }

    @Override
    public void addProject() {
        String projectName = readWord("Введите название проекта: ");
        String projectDescription = readWord("Введите описание проекта: ");
        String projectDateStart = readWord("Введите дату начала проекта: ");
        String projectDateFinish = readWord("Введите дату окончания проекта: ");
        Project project = null;
        try {
            project = new Project(projectName, projectDescription, projectDateStart, projectDateFinish);
        } catch (ParseException e) {
            log("ERROR", "Неверный формат даты начала или окончания проекта!");
            return;
        }
        projectService.persist(project);
    }

    @Override
    public void addTask() {
        String projectName = readWord("Введите название проекта: ");
        /**
         * Костыль
         */
        Project project = projectService.find(projectName);
        if (project != null) {
            String taskName = readWord("Введите название задачи: ");
            String taskDescription = readWord("Введите описание задачи: ");
            String taskDateStart = readWord("Введите дату начала выполнения задачи: ");
            String taskDateFinish = readWord("Введите дату окончания выполнения задачи: ");
            Task task = null;
            try {
                task = new Task(taskName, taskDescription, taskDateStart, taskDateFinish, project.getId());
            } catch (ParseException e) {
                log("ERROR", "Неверный формат даты начала или окончания проекта!");
                return;
            }
            taskService.persist(task);
        } else {
            log("MESSAGE", "Проект с таким названием не найден!");
        }
    }

    @Override
    public void changeProjectName() {
        String projectName = readWord("Введите название проекта: ");
        String newProjectName = readWord("Введите новое название для проекта: ");
        projectService.merge(projectName, newProjectName);
    }

    @Override
    public void changeTaskName() {
        String projectName = readWord("Введите название проекта: ");
        String taskName = readWord("Введите название задачи: ");
        String newTaskName = readWord("Введите новое название для задачи: ");
        /**
         * Костыль
         */
        Project project = projectService.find(projectName);
        if (project != null) {
            taskService.merge(project.getId(), taskName, newTaskName);
        } else {
            log("MESSAGE", "Проект с таким названием не найден!");
        }
    }

    @Override
    public void removeProject() {
        String projectName = readWord("Введите название проекта: ");
        projectService.remove(projectName);
    }

    @Override
    public void removeAllProjects() {
        projectService.removeAll();
    }

    @Override
    public void removeTask() {
        String projectName = readWord("Введите название проекта: ");
        /**
         * Костыль
         */
        Project project = projectService.find(projectName);
        if (project != null) {
            String taskName = readWord("Введите название задачи: ");
            taskService.remove(project.getId(), taskName);
        } else {
            log("MESSAGE", "Проект с таким названием не найден");
        }
    }

    @Override
    public void removeAllTasks() {
        String projectName = readWord("Введите название проекта: ");
        /**
         * Костыль
         */
        Project project = projectService.find(projectName);
        if (project != null) {
            taskService.removeAll(project.getId());
        } else {
            log("MESSAGE", "Проект с таким названием не найден!");
        }
    }

    @Override
    public String readWord(String message) {
        System.out.print(message);
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    @Override
    public void printHelp() {
        commandsList.forEach(description -> {
            System.err.println("COMMAND -> " + description);
        });
    }
}
