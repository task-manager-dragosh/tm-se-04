package ru.dragosh.tm.bootstrap;

import ru.dragosh.tm.bootstrap.intetfaces.BootstrapInterface;
import ru.dragosh.tm.bootstrap.intetfaces.CommandPerformer;
import ru.dragosh.tm.repos.ProjectRepoImpl;
import ru.dragosh.tm.repos.TaskRepoImpl;
import ru.dragosh.tm.repos.interfaces.ProjectRepo;
import ru.dragosh.tm.repos.interfaces.TaskRepo;
import ru.dragosh.tm.services.ProjectServiceImpl;
import ru.dragosh.tm.services.TaskServiceImpl;
import ru.dragosh.tm.services.interfaces.ProjectService;
import ru.dragosh.tm.services.interfaces.TaskService;
import ru.dragosh.tm.utils.OutputUtils;

import java.util.Scanner;

import static ru.dragosh.tm.utils.BootstrapUtils.*;
import static ru.dragosh.tm.utils.BootstrapUtils.EXIT;

public class Bootstrap implements BootstrapInterface {
    private boolean programRuns;
    // Interfaces:
    private CommandPerformer commandPerformer;
    private ProjectRepo projectRepo;
    private TaskRepo taskRepo;
    private ProjectService projectService;
    private TaskService taskService;

    public Bootstrap() {
        this.programRuns = true;

        this.projectRepo = new ProjectRepoImpl();
        this.taskRepo = new TaskRepoImpl();
        this.projectService = new ProjectServiceImpl(projectRepo);
        this.taskService = new TaskServiceImpl(taskRepo);

        this.commandPerformer = new CommandPerformerImpl(projectService, taskService);
    }

    @Override
    public void init() {
        System.out.println(WELCOME_MESSAGE);
        System.out.println(HELP_MESSAGE);
        while (programRuns) {
            String command = readCommand().toLowerCase();
            switch (command) {
                case HELP: {
                    commandPerformer.printHelp();
                    break;
                }
                case FIND_PROJECT: {
                    commandPerformer.findProject();
                    break;
                }
                case FIND_ALL_PROJECTS: {
                    commandPerformer.findAllProjects();
                    break;
                }
                case FIND_TASK: {
                    commandPerformer.findTask();
                    break;
                }
                case FIND_ALL_TASKS: {
                    commandPerformer.findAllTasks();
                    break;
                }
                case ADD_PROJECT: {
                    commandPerformer.addProject();
                    break;
                }
                case ADD_TASK: {
                    commandPerformer.addTask();
                    break;
                }
                case CHANGE_PROJECT_NAME: {
                    commandPerformer.changeProjectName();
                    break;
                }
                case CHANGE_TASK_NAME: {
                    commandPerformer.changeTaskName();
                    break;
                }
                case REMOVE_PROJECT: {
                    commandPerformer.removeProject();
                    break;
                }
                case REMOVE_ALL_PROJECTS: {
                    commandPerformer.removeAllProjects();
                    break;
                }
                case REMOVE_TASK: {
                    commandPerformer.removeTask();
                    break;
                }
                case REMOVE_ALL_TASKS: {
                    commandPerformer.removeAllTasks();
                    break;
                }
                case EXIT: {
                    programRuns = false;
                    OutputUtils.log("MESSAGE", "Программа завершила свою работу!");
                    break;
                }
                default: {
                    OutputUtils.log("MESSAGE", "Неверная команда!");
                    break;
                }
            }
        }
    }

    @Override
    public String readCommand() {
        System.out.print("Введите команду -> ");
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }
}
