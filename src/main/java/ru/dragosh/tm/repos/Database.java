package ru.dragosh.tm.repos;

import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;

import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.Map;

final class Database {
    public static Map<String, Project> projects = new LinkedHashMap<>();
    public static Map<String, Task> tasks = new LinkedHashMap<>();

    static {
        try {
            Project project1 = new Project("project1", "test project", "31.10.2019", "31.11.2019");
            Project project2 = new Project("project2", "test project 2", "30.10.2019", "30.11.2019");
            Project project3 = new Project("project3", "test project 3", "30.10.2019", "30.11.2019");

            projects.put(project1.getId(), project1);
            projects.put(project2.getId(), project2);
            projects.put(project3.getId(), project3);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
    }
}
