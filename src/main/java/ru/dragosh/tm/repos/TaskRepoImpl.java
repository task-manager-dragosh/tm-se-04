package ru.dragosh.tm.repos;

import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.repos.interfaces.TaskRepo;

import java.util.ArrayList;
import java.util.List;

public class TaskRepoImpl implements TaskRepo {
    @Override
    public List<Task> findAll(String projectId) {
        final List<Task> taskList = new ArrayList<>();
        Database.tasks.forEach((key, value) -> {
            if (value.getProjectId().equals(projectId))
                taskList.add(value);
        });
        return taskList;
    }

    @Override
    public Task find(String projectId, String taskName) {
        final Task[] resultTask = new Task[1];
        Database.tasks.forEach((key, value) -> {
            if (value.getProjectId().equals(projectId) && value.getNameTask().equals(taskName))
                resultTask[0] = value;
        });
        return resultTask[0];
    }

    @Override
    public void persist(Task task) {
        Database.tasks.put(task.getId(), task);
    }

    @Override
    public void merge(Task task) {
        Database.tasks.put(task.getId(), task);
    }

    @Override
    public void remove(String nameTask) {
        Database.tasks.forEach((key, value) -> {
            if (value.getNameTask().equals(nameTask))
                Database.tasks.remove(key);
        });
    }

    @Override
    public void removeAll(String projectId) {
        Database.tasks.forEach((key, value) -> {
            if (value.getProjectId().equals(projectId))
                Database.tasks.remove(key);
        });
    }
}
