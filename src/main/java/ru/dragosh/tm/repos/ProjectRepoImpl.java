package ru.dragosh.tm.repos;

import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.repos.interfaces.ProjectRepo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectRepoImpl implements ProjectRepo {
    @Override
    public List<Project> findAll() {
        final List<Project> projectList = new ArrayList<>();
        Database.projects.forEach((key, value) -> {
            projectList.add(value);
        });
        return projectList;
    }

    @Override
    public Project find(String nameProject) {
        final Project[] resultProject = {
                null
        };
        Database.projects.forEach((key, value) -> {
            if (value.getName().equals(nameProject))
                resultProject[0] = value;
        });
        return resultProject[0];
    }

    @Override
    public void persist(Project project) {
        Database.projects.put(project.getId(), project);
    }

    @Override
    public void merge(Project project) {
        Database.projects.put(project.getId(), project);
    }

    @Override
    public void remove(String nameProject) {
        String[] idProject = new String[1];
        Map<String, Project> projects = new HashMap<>(Database.projects);
        Map<String, Task> tasks = new HashMap<>(Database.tasks);
        projects.forEach((key, value) -> {
            if (value.getName().equals(nameProject)) {
                idProject[0] = key;
                Database.projects.remove(key);
            }
        });
        tasks.forEach((key, value) -> {
            if (value.getProjectId().equals(idProject[0]))
                Database.tasks.remove(key);
        });
    }

    @Override
    public void removeAll() {
        Map<String, Project> projects = new HashMap<>(Database.projects);
        Map<String, Task> tasks = new HashMap<>(Database.tasks);
        projects.forEach((keyProject, valueProject) -> {
            tasks.forEach((keyTask, valueTask) -> {
                if (valueTask.getProjectId().equals(keyProject))
                    Database.tasks.remove(keyTask);
            });
            Database.projects.remove(keyProject);
        });
    }
}
