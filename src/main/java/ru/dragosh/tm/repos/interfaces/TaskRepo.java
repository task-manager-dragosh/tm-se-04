package ru.dragosh.tm.repos.interfaces;

import ru.dragosh.tm.entity.Task;

import java.util.List;

public interface TaskRepo {
    List<Task> findAll(String projectId);
    Task find(String projectId, String nameTask);

    /**
     * Добавляет объект task в базу данных Tasks после проверки на уникальность
     * @param task - объект на добавление в базу данных Tasks
     */
    void persist(Task task);

    /**
     * Обновляет, изменяет название созданного объекта и обновляет его в базе данных Tasks
     * @param task - объект с измененным именем, который потом обнавляется в базу данных Tasks
     */
    void merge(Task task);
    void remove(String nameTask);
    void removeAll(String projectId);
}
