package ru.dragosh.tm.repos.interfaces;

import ru.dragosh.tm.entity.Project;

import java.util.List;

public interface ProjectRepo {
    List<Project> findAll();
    Project find(String projectName);

    /**
     * Добавляет объект project в базу данных Projects после проверки на уникальность
     * @param project - объект на добавление в базу данных Projects
     */
    void persist(Project project);

    /**
     * Обновляет, изменяет название созданного объекта и обновляет его в базе данных Projects
     * @param project - объект с измененным именем, который потом обнавляется в базу данных Projects
     */
    void merge(Project project);
    void remove(String projectName);
    void removeAll();
}
