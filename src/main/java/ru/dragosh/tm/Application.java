package ru.dragosh.tm;

import ru.dragosh.tm.bootstrap.Bootstrap;

public class Application {
    public static void main( String[] args ) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}