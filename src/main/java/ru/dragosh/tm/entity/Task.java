package ru.dragosh.tm.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Task {
    private String id;
    private String name;
    private String description;
    private String dateStart;
    private String dateFinish;
    private String projectId;

    public Task() {
        generateUUID();
    }

    public Task(String name, String description, String dateStart, String dateFinish, String projectId) throws ParseException {
        this();
        this.name = name;
        this.description = description;
        this.setDateStart(dateStart);
        this.setDateFinish(dateFinish);
        this.projectId = projectId;
    }

    public String getId() {
        return this.id;
    }

    private void generateUUID() {
        this.id = UUID.randomUUID().toString();
    }

    public String getNameTask() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("dd.mm.yyyy");
        this.dateStart = dt.format(dt.parse(dateStart));
    }

    public String getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(String dateFinish) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("dd.mm.yyyy");
        this.dateFinish = dt.format(dt.parse(dateFinish));
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;

        if (name == null)
            return false;

        return name.equals(task.name);
    }

    @Override
    public String toString() {
        return "UUID задачи: " + this.id + ";\n" +
                "Название задачи: " + this.name + ";\n" +
                "Описание задачи: " + this.description + ";\n" +
                "Дата начала выполнения задачи: " + this.dateStart + ";\n" +
                "Дата окончания выполнения задачи: " + this.dateFinish + ";";
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}